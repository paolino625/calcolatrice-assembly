
# PROGETTO: Architettura degli Elaboratori II - Turno B

# NOME PROGETTO: CALCOLATRICE

# STUDENTE: Paolo Calcagni

# MATRICOLA: 892870

# OBIETTIVO: Data un espressione matematica in input, il programma è in grado di calcolarne il risultato (rispettando le priorità delle operazioni e delle parentesi) e stamparlo.

# NOTE:

# 1) La calcolatrice non è pensata per gestire numeri negativi e/o decimali
# 2) Si suppone che in ingresso il programma ricevi espressioni matematiche formulate correttamente
# 3) La calcolatrice accetta esclusivamente le parentesi tonde

# ESEMPI DI FUNZIONAMENTO

# INPUT:

# 3 + 4
# 3 + 4 * 4
# (3 + 4) * 4
# (3 + 2) * (3 - 2)
# (3*2)+2
# 10 + ((20-19)-(3*4-11*1)) * 30
# 23/0

# OUTPUT:

# 7
# 19
# 28
# 5
# 8
# 10
# ERRORE: non puoi dividere un numero per 0!

	.data

	stringaletta: .space 100 # 1 byte per ogni carattere
	string1a: .asciiz "BENVENUTO! Mi presento! Sono CALC! Una calcolatrice fenomenale pronta per servirti! :)"
	string1b: .asciiz "Data un espressione matematica in input, sono capace di calcolarne il risultato e stamparlo qui!"
	string1c: .asciiz "Ti stai chiedendo cosa mi rende tanto speciale dalle altre calcolatrici?"
	string1d: .asciiz "Riesco automaticamente a dare precedenza alle operazioni di moltiplicazione e divisione!"
	string1e: .asciiz "Dici che non è abbastanza per impressionarti? Ebbene, ora ti svelo il mio asso nella manica..."
	string1f: .asciiz "Accetto anche le parentesi per espressioni particolarmente difficili!"
	string1g: .asciiz "Non vedi l'ora di provarmi, vero?"
	string1h: .asciiz "Prima però alcune avvertenze! Non vorrai mica che vada in crash e mi blocchi... :("
	string1i: .asciiz "Purtroppo non sono in grado di gestire numeri negativi o decimali... :( Quando ho chiesto al mio creatore il perché di questa ingiustizia mi ha risposto sbuffando e ha brontolato che gli avessi fatto perdere già troppo tempo..."
	string1l: .asciiz "Inoltre suppongo che chi mi utilizzi sia abbastanza intelligente da scrivere solo formule sintatticamente corrette... Tu sei una di quelle persone, vero? :D"
	string1m: .asciiz "Ora la LEGGENDA: "
	string1n: .asciiz "+ per l'operazione di somma"
	string1o: .asciiz "- per l'operazione di sottrazione"
	string1p: .asciiz "* per la moltiplicazione"
	string1q: .asciiz "/ per la divisione"
	string1r: .asciiz "() per le parentesi"
	string1s: .asciiz "Penso di averti detto tutto ora! E' giunto il momento di mettermi alla prova!"
	string1t: .asciiz "Inserisci un espressione matematica da calcolare: "
	string2: .asciiz "[CONTROLLO] Ho tolto gli spazi dall'espressione: "
	string3: .asciiz "[CONTROLLO] I caratteri ASCII della prima coppia di parentesi prese in analisi sono: "
	string4: .asciiz "[CONTROLLO] L'espressione all'interno della parentesi è: "
	string5: .asciiz "[CONTROLLO] Non ho trovato nessuna moltiplicazione."
	string6: .asciiz "[CONTROLLO] Il numero di caratteri della stringa è: "
	string7: .asciiz "[CONTROLLO] Ho trovato una moltiplicazione nella procedura CALCOLATORE. "
	string8: .asciiz "[CONTROLLO] Il risultato della moltiplicazione della procedura CALCOLATORE è: "
	string9: .asciiz "[CONTROLLO] Il risultato della procedura RISOLVOPARENTESI è: "
	string10: .asciiz "[CONTROLLO] Il risultato della procedura CALCOLATORE è: "
	string11: .asciiz "[CONTROLLO] Il numero di caratteri della stringa risultante della procedura CALCOLATORE è: "
	string12: .asciiz "[CONTROLLO] Il risultato della procedura TOLGOPARENTESI è: "
	string13: .asciiz "[CONTROLLO] Il numero di caratteri della stringa risultante della procedura TOLGOPARENTESI è: "
	string14: .asciiz "[CONTROLLO] Il risultato di NOPARENTESI è: "
	string15: .asciiz "IL RISULTATO FINALE E': "
	string16: .asciiz "[CONTROLLO] Sono entrato in NOPARENTESI"
	string17: .asciiz "[CONTROLLO] Il CALCOLATORE riceve in ingresso in $a0: "
	string18: .asciiz " e in $a1: "
	string19: .asciiz "[CONTROLLO] Risultato temporaneo della procedura NUMEROINSTRINGA: "
	string20: .asciiz "[CONTROLLO] NUMEROINSTRINGA riceve in ingresso: "
	string21: .asciiz "[CONTROLLO] Passo alla procedura MOLTIPLICAZIONE la stringa: "
	string22: .asciiz "[CONTROLLO] Passo alla procedura MOLTIPLICAZIONE la posizione del simbolo dell'operazione: "
	string23: .asciiz "[CONTROLLO] PREPAROOPERAZIONE riceve in ingresso la posizione relativa del simbolo operazione nella stringa: "
	string24: .asciiz "[CONTROLLO] PREPAROOPERAZIONE riceve in ingresso il base address della stringa: "
	string25: .asciiz "[CONTROLLO] PREPAROOPERAZIONE restituisce il numero caratteri del primo operando: "
	string26: .asciiz "[CONTROLLO] PREPAROOPERAZIONE restituisce il numero caratteri del secondo operando: "
	string27: .asciiz "[CONTROLLO] PREPAROOPERAZIONE restituisce il primo operando in forma di intero (numero): "
	string28: .asciiz "[CONTROLLO] PREPAROOPERAZIONE restituisce il secondo operando in forma di intero (numero): "
	string29: .asciiz "[CONTROLLO] MOLTIPLICAZIONE restituisce la stringa contentenente il risultato della moltiplicazione: "
	string30: .asciiz "[CONTROLLO] MOLTIPLICAZIONE restituisce il numero di caratteri della stringa: "
	string31: .asciiz "[CONTROLLO] MOLTIPLICAZIONE restituisce il numero di caratteri del primo operando: "
	string32: .asciiz "[CONTROLLO] MOLTIPLICAZIONE restituisce il numero di caratteri del secondo operando: "
	string33: .asciiz "[CONTROLLO] Non ho trovato nessuna divisione."
	string34: .asciiz "[CONTROLLO] Ho trovato una divisione nella procedura CALCOLATORE."
	string35: .asciiz "[CONTROLLO] Passo alla procedura DIVISIONE la stringa: "
	string36: .asciiz "[CONTROLLO] Passo alla procedura DIVISIONE la posizione del simbolo dell'operazione: "
	string37: .asciiz "[CONTROLLO] Il risultato della divisione della procedura CALCOLATORE è: "
	string38: .asciiz "[CONTROLLO] Ho trovato una somma nella procedura CALCOLATORE."
	string39: .asciiz "[CONTROLLO] Passo alla procedura SOMMA la stringa: "
	string40: .asciiz "[CONTROLLO] Passo alla procedura SOMMA la posizione del simbolo dell'operazione: "
	string41: .asciiz "[CONTROLLO] Il risultato della somma della procedura CALCOLATORE è: "
	string42: .asciiz "[CONTROLLO] Non ho trovato nessuna somma."
	string43: .asciiz "[CONTROLLO] Non ho trovato nessuna sottrazione."
	string44: .asciiz "[CONTROLLO] Passo alla procedura SOTTRAZIONE la stringa: "
	string45: .asciiz "[CONTROLLO] Passo alla procedura SOTTRAZIONE la posizione del simbolo dell'operazione: "
	string46: .asciiz "[CONTROLLO] Il risultato della sottrazione della procedura CALCOLATORE è: "
	string47: .asciiz "[CONTROLLO] DIVISIONE restituisce la stringa contentenente il risultato della divisione: "
	string48: .asciiz "[CONTROLLO] DIVISIONE restituisce il numero di caratteri della stringa: "
	string49: .asciiz "[CONTROLLO] DIVISIONE restituisce il numero di caratteri del primo operando: "
	string50: .asciiz "[CONTROLLO] DIVISIONE restituisce il numero di caratteri del secondo operando: "
	string51: .asciiz "[CONTROLLO] SOMMA restituisce la stringa contentenente il risultato della somma: "
	string52: .asciiz "[CONTROLLO] SOMMA restituisce il numero di caratteri della stringa: "
	string53: .asciiz "[CONTROLLO] SOMMA restituisce il numero di caratteri del primo operando: "
	string54: .asciiz "[CONTROLLO] SOMMA restituisce il numero di caratteri del secondo operando: "
	string55: .asciiz "[CONTROLLO] SOTTRAZIONE restituisce la stringa contentenente il risultato della sottrazione: "
	string56: .asciiz "[CONTROLLO] SOTTRAZIONE restituisce il numero di caratteri della stringa: "
	string57: .asciiz "[CONTROLLO] SOTTRAZIONE restituisce il numero di caratteri del primo operando: "
	string58: .asciiz "[CONTROLLO] SOTTRAZIONE restituisce il numero di caratteri del secondo operando: "
	string59: .asciiz "[CONTROLLO] ERRORE: non puoi dividere un numero per 0!"

	.text
	.globl main

main:

INTRODUZIONE:

	li $v0, 4 # Stampo stringa
	la $a0, string1a
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1b
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1c
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1d
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1e
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1f
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1g
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1h
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1i
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1l
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1m
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1n
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1o
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1p
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1q
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1r
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string1s
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

LEGGOESPRESSIONE:

	li $v0, 4 # Stampo stringa
	la $a0, string1t
	syscall

	la $a0, stringaletta # Carico in $a0 l'indirizzo dove salvare la stringa
	li $a1, 100 # Numero massimo di caratteri della stringa
	li $v0, 8 # Carico in $v0 il valore per la lettura della stringa
	syscall # Chiamo la syscall

ELIMINOSPAZIBIANCHI: # Tolgo gli spazi bianchi dall'espressione

	PreCiclo1a:
	
	li $s1, 0 # Contatore/Posizione carattere
	li $s2, 100 # Caratteri da controllare
	la $s3, stringaletta # Carico in $s3 l'indirizzo della stringaletta

	Ciclo1a: 

	lb $s4, 0($s3) # Carico in $s4 il primo carattere della stringa

	move $a0, $s4 # Sposto il primo carattere in $a0 prima di chiamare la procedura
	move $a2, $s3 # Sposto l'indirizzo della stringa (già sommato) prima di chiamare la procedura
	move $a1, $s1 # Copio contatore in $a1

	jal EliminaSpazio # Chiamo la procedura

	move $s3, $v0 # Se la procedura ha effettivamente eliminato uno spazio aggiorno $s3 ($s3 - 4) in modo tale che al prossimo ciclo punto al carattere successivo senza saltarne nessuno

	addi $s3, $s3, 1 # Passo al carattere successivo. Vado avanti di 1 byte
	addi $s1, $s1, 1 # Aumento il contatore di 1

	slt $s5, $s1, $s2 # Se $s1 < $s2 allora $s5 = 1
	bne $s5, $zero, Ciclo1a # Se $s5 è diverso da zero allo eseguo il ciclo

	# CONTROLLO: verifico che abbia cancellato gli spazi

	li $v0, 4 # Stampo stringa
	la $a0, string2
	syscall

	li $v0, 4 # Stampo stringa di controllo (stringa senza spazi)
	la $a0, stringaletta
	syscall

	# FINE CONTROLLO

	j RISOLVOPARENTESI; # Salto a RISOLVOPARENTESI

		EliminaSpazio: # Devo spostare di un posto indietro tutti i caratteri

		li $t3, 32 # 32 = Codice ASCII spazio - " "

		# $a0: primo carattere
		# $a2: indirizzo del carattere nell'espressione

		li $t2, 98 # (a causa di n+1)

		move $a3, $a2 # Copio l'indirizzo del carattere nell'espressione anche in $a3

		move $t0, $s1 # Copio in $t0 il contatore, per poterlo modificare all'interno di questo Ciclo

		bne $a0, $t3, End2EliminaSpazio	# Se i due caratteri sono diversi non devo spostare nulla, posso uscire direttamente

		Ciclo1b:

		addi $a2, $a2, 1 # Passo al carattere successivo (n+1)

		lb $t1, 0($a2) # Carico in $t1 il carattere n+1
		addi $a2, $a2, -1 # Punto allo spazio (n)
		sb $t1, 0($a2) # Carico $t1 (carattere n+1) al posto dello spazio (n)

		addi $t0, $t0, 1 # Aumento il contatore
		addi $a2, $a2, 1 # Punto al carattere successivo

		slt $t4, $t0, $t2 # Se $t0 < $t2 allora $t4 = 1
		bne $t4, $zero, Ciclo1b # Se $t4 è diverso da 0 salto a Ciclo1b

		End1EliminaSpazio:

		addi $a3, $a3, -1 # Ora tutta la frase è traslata, quindi devo considerare un carattere in meno

		move $v0, $a3 # Copio $a3 in $v0 (come richiesto dalle convenzioni delle procedure MIPS)

		jr $ra # Esco dalla procedura

		End2EliminaSpazio:

		move $v0, $a3 # Copio $a3 in $v0 (come richiesto dalle convenzioni delle procedure MIPS)

		jr $ra # Esco dalla procedura

RISOLVOPARENTESI: # trovo l'ultima parentesi "(" e mi salvo la sua posizione

	la $s0, stringaletta # Salvo base address della stringa in $s0

	PreCiclo2a:

	li $t0, 0 # Registro per capire se abbiamo trovato almeno una parentesi
	# $s0 = base address della stringa
	li $s1, 0 # Contatore/posizione carattere
	li $s2, 40 # "("
	li $s3, 41 # ")"
	li $s4, 100 # Numero massimo di caratteri

	move $t5, $s0 # Copio $s0 in $t5 dato che quando dovrò passare a NOPARENTESI devo passare il base address della stringa

	Ciclo2a:

	lb $s5, 0($s0) # Carico il carattere in $s5

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $t5, 0($sp)

	jal SalvoPosizione # Chiamo la procedura

	lw $t5, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	addi $s0, $s0, 1 # Punto al carattere successivo
	addi $s1, $s1, 1 # Aumento il contatore di 1

	slt $s6, $s1, $s4 # Se $s1 < $s4 allora $s6 = 1
	bne $s6, $zero, Ciclo2a # Se $s6 è diverso da zero salto a Ciclo2a

	beq $t0, $zero, NOPARENTESI # Se non ci sono parentesi salto a NOPARENTESI e passo in $t5 l'indirizzo della stringa

	j PreCiclo2b # Salto a PreCiclo2b

	SalvoPosizione:

	beq $s2, $s5, Salvo # Se $s2 = $s5 (se il carattere è una parentesi) salto a Salvo

	EndSalvoPosizione1:

	jr $ra # Esco dalla procedura

	Salvo:

	move $t2, $s1 # Salvo la posizione del carattere "(" perché mi serve al ciclo successivo
	move $t3, $s1 # Copio $s1 in $t3

	la $t1, stringaletta # Carico base address in $t1
	add $t3, $t3, $t1 # Aggiungo al base address la posizione del carattere interessato (per avere la posizione assoluta del carattere nella stringa)
	move $s7, $t3 # Copio la posizione della parentesi in $s7

	li $t0, 1 # Metto $t0 a 1 (perché ho trovato una parentesi)

	EndSalvoPosizione2:

	jr $ra # Esco dalla procedura

	PreCiclo2b:

	move $s0, $s7 # Salvo in $s0 la posizione della parentesi "(" appena trovata. Faccio partire la scansione della parentesi ")" da lì.
	move $s1, $t2 # Contatore/posizione carattere
	# $s3 = ")"
	# $s4 = 100 (numero massimo di caratteri)

	Ciclo2b:

	lb $s5, 0($s0) # Carico il carattere in $s5

	beq $s5, $s3, ParentesiTrovata # Se $s5 = $s3 salto a ParentesiTrovata

	addi $s0, $s0, 1 # Punto al carattere successivo
	addi $s1, $s1, 1 # Aumento il contatore di 1

	slt $s6, $s1, $s4 # Se $s1 < $s4 allora $s6 = 1
	bne $s6, $zero, Ciclo2b # Se $s6 è diverso da zero salto a Ciclo2b

	ParentesiTrovata:

	la $t0, stringaletta
	add $s1, $s1, $t0 # Aggiungo al base address la posizione del carattere interessato (per avere la posizione assoluta del carattere nella stringa)

	# $s7 = posizione della parentesi "("
	# $s1 = posizione della parentesi ")" appena trovata

	# CONTROLLO: verifico che le posizione delle parentesi trovate siano giuste

	li $v0, 4 # Stampo stringa
	la $a0, string3
	syscall

	lb $t0, 0($s7) # Carico in $t0 il carattere ASCII della parentesi "("

	li $v0, 1 # Stampo numero (posizione della prima parentesi)
	move $a0, $t0
	syscall

	lb $t1, 0($s1) # Carico in $t1 il carattere ASCII della parentesi ")"

	li $v0, 1 # Stampo numero (posizione della seconda parentesi)
	move $a0, $t1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	move $a0, $s7 # Copio in $a0 la posizione assoluta della parentesi "("
	move $a1, $s1 # Copio in $a1 la posizione assoluta della parentesi ")" appena trovata

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $t5, 0($sp)

	jal TOLGOPARENTESI # Chiamo la procedura

	lw $t5, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	# $v0 = base address della stringa risultante
	# $v1 = lunghezza della stringa
	# $s7 = posizione assoluta della parentesi "("
	# $s1 = posizione assoluta della parentesi ")"

	PreCiclo2c:

	li $t0, 0 # Azzero il contatore
	li $t3, 50 # Utile per fermare il ciclo

	move $s2, $s7 # Copio $s7 in $s2 perché modificherò $s2
	add $s2, $s2, $v1 # Sommo alla posizione assoluta della parentesi "(" la lunghezza della stringa risultante da inserire

	Ciclo2c:

	addi $s1, $s1, 1 # Punto al carattere successivo alla parentesi ")"

	lb $t1, 0($s1) # Prendo i caratteri successivi alla parentesi ")"
	sb $t1, 0($s2) # Carico i caratteri a partire dalla posizione della parentesi "(" + lunghezza della stringa risultante da inserire

	addi $s2, $s2, 1 # Punto al carattere successivo
	addi $t0, $t0, 1 # Aumento il contatore

	slt $t2, $t0, $t3 # Se $t0 < $t3 allora $t2 = 1
	bne $t2, $zero, Ciclo2c # Se $t2 è diverso da zero allora salto a Ciclo2c

	PreCiclo2d:

	# $v0 = base address della stringa risultante
	# $v1 = lunghezza della stringa
	# $s7 = posizione assoluta della parentesi "("
	# $s1 = posizione assoluta della parentesi ")"

	li $t0, 0 # Azzero il contatore
	move $t3, $s7 # Copio $s7 in $t3 perché sporcherò $s7

	Ciclo2d:

	lb $t1, 0($v0) # Inserisco la stringa risultante
	sb $t1, 0($s7)

	addi $v0, $v0, 1 # Punto al carattere successivo
	addi $s7, $s7, 1 # Punto al carattere successivo

	addi $t0, $t0, 1 # Aumento di 1 il contatore

	slt $t2, $t0, $v1 # Se $t0 è minore di $v1 allora $t2 = 1
	bne $t2, $zero, Ciclo2d # Se $t2 è diverso da 0 allora salto a Ciclo2d

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string9
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t5
	syscall

	# FINE CONTROLLO

	move $s0, $t5 # Prima di richiamare la procedura RISOLVOPARENTESI passo in $s0 l'indirizzo della nuova stringa

	j PreCiclo2a # Torno a vedere se ci sono altre parentesi

NOPARENTESI:
	
	li $v0, 4 # Stampo stringa
	la $a0, string16
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# $t5 = indirizzo della stringa

	# Se non ci sono simboli di operazioni restituisco il risultato senza passare al calcolatore

	PreCiclo8:

	li $t0, 0 # Azzero il contatore
	move $t1, $t5 # Copio in $t1 il base address della stringa

	li $t4, 50 # Stop per il ciclo

	# $t5 NON SI PUO UTILIZZARE

	Ciclo8:

	lb $t6, 0($t1) # Copio in $t6 i vari caratteri della stringa

	li $t2, 43 # Carattere ASCII del simbolo "+"
	beq $t6, $t2, SiCalcolatore # Se $t6 = $t2 allora passo a SìCalcolatore

	li $t2, 45 # Carattere ASCII del simbolo "-"
	beq $t6, $t2, SiCalcolatore # Se $t6 = $t3 allora passo a SìCalcolatore

	li $t2, 42 # Carattere ASCII del simbolo "*"
	beq $t6, $t2, SiCalcolatore # Se $t6 = $t4 allora passo a SìCalcolatore

	li $t2, 47 # Carattere ASCII del simbolo "/"
	beq $t6, $t2, SiCalcolatore # Se $t6 = $t5 allora passo a SìCalcolatore

	add $t1, $t1, 1 # Passo al carattere successivo

	addi $t0, $t0, 1 # Aumento il contatore

	slt $t3, $t0, $t4 # Se $t0 è minore di $t7 allora $a0 = 1 altrimenti $a0 = 0
	bne $t3, $zero, Ciclo8 # Se $a0 è diverso da 0 allora salto a Ciclo6
 
	FineCiclo8: # Se arrivo qui significa che nella stringa non ci sono simboli di operazioni, quindi posso stamparla così com'è

	li $v0, 4 # Stampo stringa
	la $a0, string15
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t5
	syscall

	j Exit # Esco dal programma

	SiCalcolatore:

	# Devo passare a CALCOLATORE in $a0 il base address della stringa e in $a1 il numero di caratteri della stringa, quindi devo prima calcolare il numero di caratteri della stringa

	RisultatoNumeroCaratteriStringa2:

	PreCiclo5:
	
	li $t0, 0 # Azzero il contatore
	li $t6, 10 # Carattere /n
	move $a0, $t5 # Copio il base address della stringa risultante in $a0

	Ciclo5:
	
	lb $t3, 0($t5) # Carico in $t3 i caratteri della stringa

	beq $t3, $t6, FineCiclo5 # Se il carattere contenuto in $t3 è uguale al carattere " " allora stoppo il contatore

	addi $t0, $t0, 1 # Aumento il contatore di 1
	addi $t5, $t5, 1 # Punto al carattere successivo

	j Ciclo5 # Mando il ciclo in loop fino a quando non trovo uno spazio vuoto

	FineCiclo5:

	move $a1, $t0 # Copio in $a1 il numero di caratteri della stringa

	jal CALCOLATORE # Chiamo procedura

	# $v0 = stringa risultante
	# $v1 = numero dei caratteri di tale stringa

	move $t0, $v0 # Copio $v0 in $t0
	move $t1, $v1 # Copio $v1 in $t1

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string14
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	StampoRisultatoFinale:

	li $v0, 4 # Stampo stringa
	la $a0, string15
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	j Exit # Esco dal programma

TOLGOPARENTESI:

# $a0 = posizione assoluta della parentesi "("
# $a1 = posizione assoluta della parentesi ")"

	move $t1, $a0 # Copio la posizione assoluta della parentesi "(" in $t1 perché poi dovrò modificare il registro $a0
	move $t2, $a1 # Copio la posizione assoluta della parentesi ")" in $t2

	sub $t0, $t2, $t1 # Devo capire di quanti caratteri è la stringa in ingresso, quindi innanzitutto effettuo la differenza tra le due posizioni
	addi $t0, $t0, -1 # $t0 = $t0 - 1 - [Perché? Assumiamo che le parentesi abbiano rispettivamente posizione 4 e 9. La differenza tra le due posizioni risulta 5 ma in realtà sono 4 i caratteri contenuti all'interno di esse.]

	li $v0, 9 # Carico il valore in $v0 per sbkr
	li $a0, 100 # Chiedo uno spazio di memoria sufficientemente ampio
	syscall 
	# Ho in $v0 l'address del nuovo spazio di memoria

	PreCiclo3a: # Eseguo un ciclo per trasferire i caratteri tra le due parentesi in questo nuovo spazio allocato in memoria

	move $t6, $v0 # Copio in $t6 l'indirizzo base della nuova stringa dato che poi andrò a modificare $v0

	# $t1 = posizione assoluta della parentesi "("
	# $t2 = posizione assoluta della parentesi ")"
	# $v0 = address del nuovo spazio di memoria
	# $t0 = caratteri tra le due parentesi

	li $t4, 0 # Contatore

	Ciclo3a:

	addi $t1, $t1, 1 # Punto al carattere successivo (al primo ciclo sarà il carattere dopo la prima parentesi)

	lb $t3, 0($t1) # Carico temporaneamente il carattere preso in esame in un registro
	sb $t3, 0($v0) # Carico il carattere nel nuovo spazio di memoria

	addi $v0, $v0, 1 # Punto al carattere successivo del nuovo spazio di memoria
	addi $t4, $t4, 1 # Aumento il contatore di 1

	slt $t5, $t4, $t0 # Se $t4 < $t0 allora $t5 = 1
	bne $t5, $zero, Ciclo3a # Se $t5 è diverso da 0 salto a Ciclo3a

	# CONTROLLO: verifico che abbia prelevato la stringa giusta

	li $v0, 4 # Stampo stringa
	la $a0, string4
	syscall

	li $v0, 4 # Stampo stringa (espressione dentro le parentesi)
	move $a0, $t6
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string6
	syscall

	li $v0, 1 # Stampo il numero di caratteri della nuova stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	move $a0, $t6 # Copio in $a0 il base address della nuova stringa prima di chiamare la procedura
	move $a1, $t0 # Copio in $a1 il numero di caratteri della nuova stringa

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $ra, 0($sp)

	jal CALCOLATORE # Chiamo procedura

	# Devo ricevere da Calcolatore l'indirizzo della stringa risultante e il numero di caratteri di tale stringa

	# $v0 = base address della nuova stringa
	# $v1 = numero di caratteri della nuova stringa

	# CONTROLLO

	move $t0, $v0 # Copio base address della nuova stringa in $t0
	move $t1, $v1 # Copio numero di caratteri della nuova stringa in $t1

	li $v0, 4 # Stampo stringa
	la $a0, string12
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string13
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $v0, $t0 # Copio $t0 in $v0
	move $v1, $t1 # Copio $t1 in $v1

	# FINE CONTROLLO

	# Devo passare in $v0 il risultato sotto forma di stringa e in $v1 la lunghezza della stringa
	
	lw $ra, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	jr $ra # Esco dalla procedura

CALCOLATORE: 

	sub $sp, $sp, 36 # Mi servono più registri: salvo nello stack i registri s dato che per convenzione la procedura non deve toccarli ma può utilizzare solo i registri temporanei

	sw $s0, 32($sp) # Push
	sw $s1, 28($sp)
	sw $s2, 24($sp)
	sw $s3, 20($sp)
	sw $s4, 16($sp)
	sw $s5, 12($sp)
	sw $s6, 8($sp)
	sw $s7, 4($sp)
	sw $ra, 0($sp)

	# $a0 = base address della stringa senza parentesi
	# $a1 = numero di caratteri della stringa

	# CONTROLLO

	move $t0, $a0 # Copio $a0 in $t0
	move $t1, $a1 # Copio $a1 in $t1

	li $v0, 4 # Stampo stringa
	la $a0, string17
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string18
	syscall 

	li $v0, 1 # Stampo intero
	move $a0, $a1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $a0, $t0 # Copio $t0 in $a0
	move $a1, $t1 # Copio $t1 in $a1

	# FINE CONTROLLO

	CapiscoPriorita:

	# $a0 = base address della stringa senza parentesi
	# $a1 = numero di caratteri della stringa
	move $s0, $a0 # Copio $a0 in $s0 (base address della stringa senza parentesi)

	PreCiclo13:

	li $t0, 0 # Azzero il contatore
	move $t1, $s0 # Copio $s0 in $t1 ($t1 = base address della stringa senza parentesi)
	li $t2, 50 # Utile per fermare il ciclo
	li $t3, 43 # $t3 = "+"
	li $t4, 45 # $t4 = "-"
	li $t5, 42 # $t5 = "*"
	li $t6, 47 # $t6 = "/"

	Ciclo13:

	lb $t7, 0($t1) # Carico in $t7 i caratteri della stringa

	beq $t7, $t5, PassoMoltiplicazione # Se trovo il simbolo "*" salto a PassoMoltiplicazione
	beq $t7, $t6, PassoDivisione # Se trovo il simbolo "/" salto a PassoDivisione

	addi $t0, $t0, 1 # Aumento il contatore
	addi $t1, $t1, 1 # Punto al carattere successivo

	slt $a0, $t0, $t2 # Se $t0 < $t2 allora $a0 = 1
	bne $a0, $zero, Ciclo13 # Se $a0 è diverso da 0 salto a Ciclo13

	PreCiclo14:

	li $t0, 0 # Azzero il contatore
	move $t1, $s0 # Copio $s0 in $t1

	Ciclo14:

	lb $t7, 0($t1) # Carico in $t7 i caratteri della stringa

	beq $t7, $t3, PassoSomma # Se trovo il simbolo "+" salto a PassoSomma
	beq $t7, $t4, PassoSottrazione # Se trovo il simbolo "-" salto a PassoSottrazione

	addi $t0, $t0, 1 # Aumento il contatore
	addi $t1, $t1, 1 # Punto al carattere successivo

	slt $a0, $t0, $t2 # Se $t0 < $t2 allora $a0 = 1
	bne $a0, $zero, Ciclo14 # Se $a0 è diverso da 0 salto a Ciclo13

	j RisultatoStringa # Se sono arrivato qui non ci sono più somme o sottrazioni: passo a RisultatoStringa

	PassoMoltiplicazione:

	move $a0, $s0 # Copio $s0 in $a0 
	# $a1 è ancora intatto: non è stato sporcato

	j PreMoltiplicazione # Salto a PreMoltiplicazione

	PassoDivisione:

	move $a0, $s0 # Copio $s0 in $a0
	# $a1 è ancora intatto: non è stato sporcato

	j PreDivisione  # Salto a PreDivisione

	PassoSomma:

	move $a0, $s0 # Copio $s0 in $a0
	# $a1 è ancora intatto: non è stato sporcato

	j PreSomma  # Salto a PreDivisione

	PassoSottrazione:

	move $a0, $s0 # Copio $s0 in $a0
	# $a1 è ancora intatto: non è stato sporcato

	j PreSottrazione  # Salto a PreDivisione

	PreMoltiplicazione:

	move $s2, $a0 # Copio in $s2 il base address della stringa
	move $s3, $a0 # Copio in $s3 il base address della stringa

	li $a1, 50 # Per fermare il ciclo
	li $t2, 0 # Contatore
	li $t1, 42 # Carico in $t1 il valore ASCII di "*"

	Moltiplicazione:

	lb $t0, 0($s2) # Carico in $t0 un carattere della stringa

	beq $t0, $t1, AvvioMoltiplicazione # Se $t0 = "*" allora salto ad AvvioMoltiplicazione

	addi $s2, $s2, 1 # Punto al carattere successivo
	addi $t2, $t2, 1 # Aumento il contatore di 1

	slt $t3, $t2, $a1 # Se $t2 < $a1 allora $t3 = 1
	bne $t3, $zero, Moltiplicazione # Se $t3 è diverso da 0 allora salto a Moltiplicazione

	# INIZIO CONTROLLO

	li $v0, 4 # Stampo stringa di controllo
	la $a0, string5
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	move $a0, $s3 # Copio $s3 in $a0

	j PreDivisione # Salto a Divisione

	AvvioMoltiplicazione:

	li $v0, 4 # Stampo stringa di controllo
	la $a0, string7
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $s0, $t2 # Copio in $s0 la posizione dell'operatore "*" (posizione relativa)

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $ra, 0($sp)

	# Devo passare alla procedura moltiplicazione la posizione dell'operando nella stringa in $a0 e in $a1 la stringa

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string21
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $s3
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string22
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	move $a0, $t2 # Copio in $a0 la posizione relativa del simbolo operazione
	move $a1, $s3 # Copio in $a1 il base address della stringa

	jal MOLTIPLICAZIONE # Chiamo procedura

	lw $ra, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	# $v0 = stringa contenente il risultato della moltiplicazione
	# $v1 = lunghezza della stringa
	# $a0 = numero di caratteri del primo operando
	# $a1 = numero di caratteri del secondo operando
	# $s0 = Posizione dell'operatore "*" (posizione relativa)

	move $t0, $s0 # NUOVO # Copio in $t0 la posizione dell'operatore $s0 

	sub $s0, $s0, $a0 # Sposto il contatore/puntatore-carattere di n posti indietro (dove n sta per il numero di caratteri del primo operando) per avere la posizione realtiva del primo carattere del primo operando

	add $s1, $t0, $a1  # Sommmo alla posizione relativa di "*" il numero di caratteri del secondo operando per avere la posizione relativa dell'ultimo carattere del secondo operando

	PreCiclo4a:

	# $s0 = posizione relativa del primo carattere del primo operando
	# $s1 = posizione relativa dell'ultimo carattere del secondo operando
	# $s2 = base address della stringa
	# $s3 = numero di caratteri della stringa iniziale

	# Calcolo indirizzi assoluti

	move $t2, $s0 # Copio in $t2 la posizione relativa del primo carattere del primo operando

	move $t3, $s1 # Copio in $t3 la posizione relativa dell'ultimo carattere del secondo operando

	add $s0, $s0, $s3 # $s0 = posizione assoluta del primo carattere del primo operando
	add $s1, $s1, $s3 # $s1 = posizione assoluta dell'ultimo carattere del secondo operando

	li $t2, 0 # Azzero il contatore
	li $t5, 50 # Per fermare il contatore
	add $s4, $s0, $v1 # $s4 = posizione assoluta del primo carattere + spazio per la nuova stringa

	Ciclo4a:

	addi $s1, $s1, 1 # Punto al carattere successivo dell'ultimo carattere del secondo operando

	lb $t0, 0($s1) # Carico carattere in $t0 (registro temporaneo)
	sb $t0, 0($s4) # Carico il carattere a partire dalla posizione del primo carattere + spazio per la nuova stringa

	addi $s4, $s4, 1 # Punto al carattere successivo

	addi $t2, $t2, 1 # Aumento il contatore

	slt $t4, $t2, $t5 # Se $t2 < $t3 allora $t4 = 1
	bne $t4, $zero, Ciclo4a # Se $t4 è diverso da 0 allora salto a Ciclo4

	PreCiclo4b:

	# $v0 = base address della stringa contenente il risultato della moltiplicazione
	# $v1 = numero di caratteri della stringa da inserire
	# $s0 = posizione assoluta del primo carattere del primo operando

	li $t0, 0 # Azzero il contatore 
	li $t1, 0 # Azzero $t1 
	# move $t2, $s0 # Copio in $t2 la posizione assoluta del primo carattere del primo operando
	move $t2, $s3 # Copio $s3 in $t2

	Ciclo4b:

	lb $t3, 0($v0) # Carico carattere di $v0 nella parte vuota della stringa
	sb $t3, 0($s0) # Carico i caratteri nella stringa

	addi $v0, $v0, 1 # Punto al carattere successivo
	addi $s0, $s0, 1 # Punto al carattere successivo 
	addi $t0, $t0, 1 # Aumento il contatore

	slt $t1, $t0, $v1 # Se $t0 è minore di $v1 allora $t1 = 1
	bne $t1, $zero, Ciclo4b # Se $t1 è diverso da 0 allora salto a Ciclo4b

	FineMoltiplicazione:

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string8
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO
	
	# In $t2 ho il base address della stringa risultante

	move $s7, $t2 # Copio il base address della stringa risultante in $s7 ATTENZIONE $S7 DEVE RIMANERE INTATTO FINO ALLA FINE DELLA PROCEDURA CALCOLATORE perché mi serve a "RestituisciRisultato"
	move $a0, $t2 # Copio il base address della stringa risultante in $s2

	j CapiscoPriorita # Salto a CapiscoPriorita

	# --------------------------------------------------------------------------

	PreDivisione:

	move $s2, $a0 # Copio in $s2 il base address della stringa
	move $s3, $a0 # Copio in $s3 il base address della stringa

	li $a1, 50 # Per fermare il ciclo
	li $t2, 0 # Contatore
	li $t1, 47 # Carico in $t1 il valore ASCII di "/"

	Divisione:

	lb $t0, 0($s2) # Carico in $t0 un carattere della stringa

	beq $t0, $t1, AvvioDivisione # Se $t0 = "/" allora salto ad AvvioDivisione

	addi $s2, $s2, 1 # Punto al carattere successivo
	addi $t2, $t2, 1 # Aumento il contatore di 1

	slt $t3, $t2, $a1 # Se $t2 < $a1 allora $t3 = 1
	bne $t3, $zero, Divisione # Se $t3 è diverso da 0 allora salto a Divisione

	# INIZIO CONTROLLO

	li $v0, 4 # Stampo stringa di controllo
	la $a0, string33
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	move $a0, $s3 # Copio $s3 in $a0

	j PreSomma # Salto a PreSomma

	AvvioDivisione:

	li $v0, 4 # Stampo stringa di controllo
	la $a0, string34
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $s0, $t2 # Copio in $s0 la posizione dell'operatore "/" (posizione relativa)

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $ra, 0($sp)

	# Devo passare alla procedura moltiplicazione la posizione dell'operando nella stringa in $a0 e in $a1 la stringa

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string35
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $s3
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string36
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	move $a0, $t2 # Copio in $a0 la posizione relativa del simbolo operazione
	move $a1, $s3 # Copio in $a1 il base address della stringa

	jal DIVISIONE # Chiamo la procedura

	lw $ra, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	# $v0 = stringa contenente il risultato della moltiplicazione
	# $v1 = lunghezza della stringa
	# $a0 = numero di caratteri del primo operando
	# $a1 = numero di caratteri del secondo operando
	# $s0 = Posizione dell'operatore "/" (posizione relativa)

	move $t0, $s0 # Copio la posizione dell'operatore $s0 in $t0

	sub $s0, $s0, $a0 # Sposto il contatore/puntatore-carattere di n posti indietro (dove n sta per il numero di caratteri del primo operando) per avere la posizione realtiva del primo carattere del primo operando

	add $s1, $t0, $a1 # Sommmo alla posizione relativa di "/" il numero di caratteri del secondo operando per avere la posizione relativa dell'ultimo carattere del secondo operando

	PreCiclo9a:

	# $s0 = posizione relativa del primo carattere del primo operando
	# $s1 = posizione relativa dell'ultimo carattere del secondo operando
	# $s2 = base address della stringa
	# $s3 = numero di caratteri della stringa iniziale

	# Calcolo indirizzi assoluti

	move $t2, $s0 # Copio in $t2 la posizione relativa del primo carattere del primo operando

	move $t3, $s1 # Copio in $t3 la posizione relativa dell'ultimo carattere del secondo operando

	add $s0, $s0, $s3 # $s0 = posizione assoluta del primo carattere del primo operando
	add $s1, $s1, $s3 # $s1 = posizione assoluta dell'ultimo carattere del secondo operando

	li $t2, 0 # Azzero il contatore
	li $t5, 50 # Per fermare il contatore
	add $s4, $s0, $v1 # $s4 = posizione assoluta del primo carattere + spazio per la nuova stringa

	Ciclo9a:

	addi $s1, $s1, 1 # Punto al carattere successivo dell'ultimo carattere del secondo operando

	lb $t0, 0($s1) # Carico carattere in $t0 (registro temporaneo)
	sb $t0, 0($s4) # Carico il carattere a partire dalla posizione del primo carattere + spazio per la nuova stringa

	addi $s4, $s4, 1 # Punto al carattere successivo

	addi $t2, $t2, 1 # Aumento il contatore

	slt $t4, $t2, $t5 # Se $t2 è minore di $t3 allora $t4 = 1
	bne $t4, $zero, Ciclo9a # Se $t4 è diverso da 0 allora salto a Ciclo9a

	PreCiclo9b:

	# $v0 = base address della stringa contenente il risultato della moltiplicazione
	# $v1 = numero di caratteri della stringa da inserire
	# $s0 = posizione assoluta del primo carattere del primo operando

	li $t0, 0 # Azzero il contatore 
	li $t1, 0 # Azzero $t1 
	move $t2, $s3 # Copio $s3 in $t2

	Ciclo9b:

	lb $t3, 0($v0) # Carico carattere di $v0 nella parte vuota della stringa
	sb $t3, 0($s0) # Carico i caratteri nella stringa

	addi $v0, $v0, 1 # Punto al carattere successivo
	addi $s0, $s0, 1 # Punto al carattere successivo 
	addi $t0, $t0, 1 # Aumento il contatore

	slt $t1, $t0, $v1 # Se $t0 < $v1 allora $t1 = 1
	bne $t1, $zero, Ciclo9b # Se $t1 è diverso da 0 allora salto a Ciclo9b

	FineDivisione:

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string37
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO
	
	# In $t2 ho il base address della stringa risultante

	move $s7, $t2 # Copio il base address della stringa risultante in $s7 ATTENZIONE $S7 DEVE RIMANERE INTATTO FINO ALLA FINE DELLA PROCEDURA CALCOLATORE perché mi serve a "RestituisciRisultato"
	move $a0, $t2 # Copio il base address della stringa risultante in $s2

	j CapiscoPriorita

	# --------------------------------------------------------------------

	PreSomma:

	move $s2, $a0 # Copio in $s2 il base address della stringa
	move $s3, $a0 # Copio in $s3 il base address della stringa

	li $a1, 50 # Per fermare il ciclo
	li $t2, 0 # Contatore
	li $t1, 43 # Carico in $t1 il valore ASCII di "+"

	Somma:

	lb $t0, 0($s2) # Carico in $t0 un carattere della stringa

	beq $t0, $t1, AvvioSomma # Se $t0 = "+" allora salto ad AvvioSomma

	addi $s2, $s2, 1 # Punto al carattere successivo
	addi $t2, $t2, 1 # Aumento il contatore di 1

	slt $t3, $t2, $a1 # Se $t2 < $a1 allora $t3 = 1
	bne $t3, $zero, Somma # Se $t3 è diverso da 0 allora salto a Ciclo4a

	# CONTROLLO

	li $v0, 4 # Stampo stringa di controllo
	la $a0, string42
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	move $a0, $s3 # Copio $s3 in $a0

	j PreSottrazione # Salto a PreSottrazione

	AvvioSomma:

	li $v0, 4 # Stampo stringa di controllo
	la $a0, string38
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $s0, $t2 # Copio in $s0 la posizione dell'operatore "+" (posizione relativa)

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $ra, 0($sp)

	# Devo passare alla procedura moltiplicazione la posizione dell'operando nella stringa in $a0 e in $a1 la stringa

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string39
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $s3
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string40
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	move $a0, $t2 # Copio in $a0 la posizione relativa del simbolo operazione
	move $a1, $s3 # Copio in $a1 il base address della stringa

	jal SOMMA # Chiamo procedura

	lw $ra, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	# $v0 = stringa contenente il risultato della moltiplicazione
	# $v1 = lunghezza della stringa
	# $a0 = numero di caratteri del primo operando
	# $a1 = numero di caratteri del secondo operando
	# $s0 = Posizione dell'operatore "/" (posizione relativa)

	move $t0, $s0 # Copio la posizione dell'operatore $s0 in $t0

	sub $s0, $s0, $a0 # Sposto il contatore/puntatore-carattere di n posti indietro (dove n sta per il numero di caratteri del primo operando) per avere la posizione realtiva del primo carattere del primo operando

	add $s1, $t0, $a1 # Sommmo alla posizione relativa di "/" il numero di caratteri del secondo operando per avere la posizione relativa dell'ultimo carattere del secondo operando

	PreCiclo11a:

	# $s0 = posizione relativa del primo carattere del primo operando
	# $s1 = posizione relativa dell'ultimo carattere del secondo operando
	# $s2 = base address della stringa
	# $s3 = numero di caratteri della stringa iniziale

	# Calcolo indirizzi assoluti

	move $t2, $s0 # Copio in $t2 la posizione relativa del primo carattere del primo operando

	move $t3, $s1 # Copio in $t3 la posizione relativa dell'ultimo carattere del secondo operando

	add $s0, $s0, $s3 # $s0 = posizione assoluta del primo carattere del primo operando
	add $s1, $s1, $s3 # $s1 = posizione assoluta dell'ultimo carattere del secondo operando

	li $t2, 0 # Azzero il contatore
	li $t5, 50 # Per fermare il contatore
	add $s4, $s0, $v1 # $s4 = posizione assoluta del primo carattere + spazio per la nuova stringa

	Ciclo11a:

	addi $s1, $s1, 1 # Punto al carattere successivo dell'ultimo carattere del secondo operando

	lb $t0, 0($s1) # Carico carattere in $t0 (registro temporaneo)
	sb $t0, 0($s4) # Carico il carattere a partire dalla posizione del primo carattere + spazio per la nuova stringa

	addi $s4, $s4, 1 # Punto al carattere successivo

	addi $t2, $t2, 1 # Aumento il contatore

	slt $t4, $t2, $t5 # Se $t2 è minore di $t3 allora $t4 = 1
	bne $t4, $zero, Ciclo11a # Se $t4 è diverso da 0 allora salto a Ciclo11a

	PreCiclo11b:

	# $v0 = base address della stringa contenente il risultato della moltiplicazione
	# $v1 = numero di caratteri della stringa da inserire
	# $s0 = posizione assoluta del primo carattere del primo operando

	li $t0, 0 # Azzero il contatore 
	li $t1, 0 # Azzero $t1 
	# move $t2, $s0 # Copio in $t2 la posizione assoluta del primo carattere del primo operando
	 move $t2, $s3

	Ciclo11b:

	lb $t3, 0($v0) # Carico carattere di $v0 nella parte vuota della stringa
	sb $t3, 0($s0) # Carico i caratteri nella stringa

	addi $v0, $v0, 1 # Punto al carattere successivo
	addi $s0, $s0, 1 # Punto al carattere successivo 
	addi $t0, $t0, 1 # Aumento il contatore

	slt $t1, $t0, $v1 # Se $t0 è minore di $v1 allora $t1 = 1
	bne $t1, $zero, Ciclo11b # Se $t1 è diverso da 0 allora salto a Ciclo11b

	FineSomma:

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string41
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO
	
	# In $t2 ho il base address della stringa risultante

	move $s7, $t2 # Copio il base address della stringa risultante in $s7 ATTENZIONE $S7 DEVE RIMANERE INTATTO FINO ALLA FINE DELLA PROCEDURA CALCOLATORE perché mi serve a "RestituisciRisultato"
	move $a0, $t2 # Copio il base address della stringa risultante in $s2

	j CapiscoPriorita # Torno a vedere se sono presenti altre somme

	# --------------------------------------------------------------------

	PreSottrazione:

	move $s2, $a0 # Copio in $s2 il base address della stringa
	move $s3, $a0 # Copio in $s3 il base address della stringa

	li $a1, 50 # Per fermare il ciclo
	li $t2, 0 # Contatore
	li $t1, 45 # Carico in $t1 il valore ASCII di "-"

	Sottrazione:

	lb $t0, 0($s2) # Carico in $t0 un carattere della stringa

	beq $t0, $t1, AvvioSottrazione # Se $t0 = "-" allora salto ad AvvioSottrazione

	addi $s2, $s2, 1 # Punto al carattere successivo
	addi $t2, $t2, 1 # Aumento il contatore di 1

	slt $t3, $t2, $a1 # Se $t2 < $a1 allora $t3 = 1
	bne $t3, $zero, Sottrazione # Se $t3 è diverso da 0 allora salto a Ciclo4a

	# CONTROLLO

	li $v0, 4 # Stampo stringa di controllo
	la $a0, string43
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	j RisultatoStringa # Salto a RisultatoStringa

	AvvioSottrazione:

	li $v0, 4 # Stampo stringa di controllo
	la $a0, string38
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $s0, $t2 # Copio in $s0 la posizione dell'operatore "+" (posizione relativa)

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $ra, 0($sp)

	# Devo passare alla procedura moltiplicazione la posizione dell'operando nella stringa in $a0 e in $a1 la stringa

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string44
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $s3
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string45
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	move $a0, $t2 # Copio in $a0 la posizione relativa del simbolo operazione
	move $a1, $s3 # Copio in $a1 il base address della stringa

	jal SOTTRAZIONE # Chiamo procedura

	lw $ra, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	# $v0 = stringa contenente il risultato della moltiplicazione
	# $v1 = lunghezza della stringa
	# $a0 = numero di caratteri del primo operando
	# $a1 = numero di caratteri del secondo operando
	# $s0 = Posizione dell'operatore "/" (posizione relativa)

	move $t0, $s0 # Copio in $t0 la posizione dell'operatore $s0 

	sub $s0, $s0, $a0 # Sposto il contatore/puntatore-carattere di n posti indietro (dove n sta per il numero di caratteri del primo operando) per avere la posizione realtiva del primo carattere del primo operando

	add $s1, $t0, $a1 # Sommmo alla posizione relativa di "/" il numero di caratteri del secondo operando per avere la posizione relativa dell'ultimo carattere del secondo operando

	PreCiclo12a:

	# $s0 = posizione relativa del primo carattere del primo operando
	# $s1 = posizione relativa dell'ultimo carattere del secondo operando
	# $s2 = base address della stringa
	# $s3 = numero di caratteri della stringa iniziale

	# Calcolo indirizzi assoluti

	move $t2, $s0 # Copio in $t2 la posizione relativa del primo carattere del primo operando

	move $t3, $s1 # Copio in $t3 la posizione relativa dell'ultimo carattere del secondo operando

	add $s0, $s0, $s3 # $s0 = posizione assoluta del primo carattere del primo operando
	add $s1, $s1, $s3 # $s1 = posizione assoluta dell'ultimo carattere del secondo operando

	li $t2, 0 # Azzero il contatore
	li $t5, 50 # Per fermare il contatore
	add $s4, $s0, $v1 # $s4 = posizione assoluta del primo carattere + spazio per la nuova stringa

	Ciclo12a:

	addi $s1, $s1, 1 # Punto al carattere successivo dell'ultimo carattere del secondo operando

	lb $t0, 0($s1) # Carico carattere in $t0 (registro temporaneo)
	sb $t0, 0($s4) # Carico il carattere a partire dalla posizione del primo carattere + spazio per la nuova stringa

	addi $s4, $s4, 1 # Punto al carattere successivo

	addi $t2, $t2, 1 # Aumento il contatore

	slt $t4, $t2, $t5 # Se $t2 < $t3 allora $t4 = 1
	bne $t4, $zero, Ciclo12a # Se $t4 è diverso da 0 allora salto a Ciclo12a

	PreCiclo12b:

	# $v0 = base address della stringa contenente il risultato della moltiplicazione
	# $v1 = numero di caratteri della stringa da inserire
	# $s0 = posizione assoluta del primo carattere del primo operando

	li $t0, 0 # Azzero il contatore 
	li $t1, 0 # Azzero $t1 
	# move $t2, $s0 # Copio in $t2 la posizione assoluta del primo carattere del primo operando
	move $t2, $s3

	Ciclo12b:

	lb $t3, 0($v0) # Carico carattere di $v0 nella parte vuota della stringa
	sb $t3, 0($s0) # Carico i caratteri nella stringa

	addi $v0, $v0, 1 # Punto al carattere successivo
	addi $s0, $s0, 1 # Punto al carattere successivo 
	addi $t0, $t0, 1 # Aumento il contatore

	slt $t1, $t0, $v1 # Se $t0 è minore di $v1 allora $t1 = 1
	bne $t1, $zero, Ciclo12b # Se $t1 è diverso da 0 allora salto a Ciclo11b

	FineSottrazione:

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string46
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO
	
	# In $t2 ho il base address della stringa risultante

	move $s7, $t2 # Copio il base address della stringa risultante in $s7 ATTENZIONE $S7 DEVE RIMANERE INTATTO FINO ALLA FINE DELLA PROCEDURA CALCOLATORE perché mi serve a "RestituisciRisultato"
	move $a0, $t2 # Copio il base address della stringa risultante in $s2

	j CapiscoPriorita # Torno a vedere se sono presenti altre sottrazioni

	RisultatoStringa:

	RisultatoNumeroCaratteriStringa: # Devo passare anche il numero di caratteri della stringa

	# Devo capire quanti caratteri contiene. Ovvero fino a quando non trovo spazi vuoti. Carattere ASCII: 32

	PreCiclo10:

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string10
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $s7
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	# $s7 = stringa
	li $t0, 0 # Azzero il contatore
	li $t1, 10 # Carico 10 in $t1

	move $t4, $s7 # Copio il base address della stringa risultante in $t4

	Ciclo10:
	
	lb $t3, 0($s7) # Carico in $t3 i caratteri della stringa

	beq $t3, $zero, FineCiclo10 # Se $t3 = 0 allora stoppo il contatore
	beq $t3, $t1, FineCiclo10 # Se $t3 = $t1 salto a FineCiclo10

	addi $t0, $t0, 1 # Aumento il contatore di 1
	addi $s7, $s7, 1 # Punto al carattere successivo

	j Ciclo10 # Mando il ciclo in loop fino a quando non trovo uno spazio vuoto

	FineCiclo10:

	move $v1, $t0 # Copio in $v1 il numero di caratteri della stringa

	# CONTROLLO

	li $v0, 4 # Stampo stringa
	la $a0, string11
	syscall

	li $v0, 1 # Stampo numero
	move $a0, $v1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	# FINE CONTROLLO

	move $v0, $t4 # Copio $t4 in $v0 (base address della stringa risultante)

	# Devo restituire la stringa risultante in $v0 e il numero di carattere di tale stringa in $v1

	j EscoDaCalcolatore;

MOLTIPLICAZIONE:

	# $a0 = Posizione relativa del simbolo operazione nella stringa
	# $a1 = Base address della stringa

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $ra, 0($sp)

	jal PREPAROOPERAZIONE # Chiamo la procedura

	lw $ra, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	# Dalla procedura ottengo i seguenti registri:

	# $v0 = numero caratteri del primo operando
	# $v1 = numero caratteri del secondo operando
	# $a0 = primo operando in forma di intero (numero)
	# $a1 = secondo operando in forma di intero (numero)

	mul $t0, $a0, $a1 # Salvo in $t0 il risultato della moltiplicazione tra $a0 e $a1

	move $a0, $t0 # Copio $t0 in $a0

	move $t1, $v0 # $t1 = numero caratteri del primo operando
	move $t2, $v1 # $t2 = numero di caratteri del secondo operando

	sub $sp, $sp, 36 # Mi servono più registri: salvo nello stack i registri s dato che per convenzione la procedura non deve toccarli ma può utilizzare solo i registri temporanei

	sw $t0, 32($sp) # Push
	sw $t1, 28($sp)
	sw $t2, 24($sp)
	sw $t3, 20($sp)
	sw $t4, 16($sp)
	sw $t5, 12($sp)
	sw $t6, 8($sp)
	sw $t7, 4($sp)
	sw $ra, 0($sp)

	jal NUMEROINSTRINGA # Chiamo la procedura

	lw $t0, 32($sp) # Recupero i dati dallo stack (Pop)
	lw $t1, 28($sp)
	lw $t2, 24($sp)
	lw $t3, 20($sp)
	lw $t4, 16($sp)
	lw $t5, 12($sp)
	lw $t6, 8($sp)
	lw $t7, 4($sp)
	lw $ra, 0($sp)

	add $sp, $sp, 36

	# $v0 = base address della stringa risultante
	# $v1 = numero di caratteri della stringa risultante

	# CONTROLLO

	move $t0, $v0 # Copio $v0 in $t0

	li $v0, 4 # Stampo stringa
	la $a0, string19
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string29
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string30
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $v1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string31
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string32
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $v0, $t0 # Copio $t0 in $v0  

	# FINE CONTROLLO

	# Devo restituire i seguenti parametri

	# $v0 = stringa contenente il risultato della moltiplicazione
	# $v1 = lunghezza della stringa
	# $a0 = numero di caratteri del primo operando
	# $a1 = numero di caratteri del secondo operando

	move $a0, $t1 # Copio $t1 in $v0
	move $a1, $t2 # Copio $t2 in $v1
	# $v0 = stringa contenete il risultato della moltiplicazione
	# $v1 = numero di caratteri della stringa

	jr $ra # Esco dalla procedura

DIVISIONE:

	# $a0 = Posizione relativa del simbolo operazione nella stringa
	# $a1 = Base address della stringa

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $ra, 0($sp)

	jal PREPAROOPERAZIONE # Chiamo la procedura

	lw $ra, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	# Dalla procedura ottengo i seguenti registri:

	# $v0 = numero caratteri del primo operando
	# $v1 = numero caratteri del secondo operando
	# $a0 = primo operando in forma di intero (numero)
	# $a1 = secondo operando in forma di intero (numero)

	beq $a1, $zero, DENOMINATORE0 # Se $a1 = 0 salto a DENOMINATORE0

	div $t0, $a0, $a1 # Salvo in $t0 il risultato della divisione tra $a0 e $a1

	move $a0, $t0 # Copio $t0 in $a0

	move $t1, $v0 # $t1 = numero caratteri del primo operando
	move $t2, $v1 # $t2 = numero di caratteri del secondo operando

	sub $sp, $sp, 36 # Mi servono più registri: salvo nello stack i registri s dato che per convenzione la procedura non deve toccarli ma può utilizzare solo i registri temporanei

	sw $t0, 32($sp) # Push
	sw $t1, 28($sp)
	sw $t2, 24($sp)
	sw $t3, 20($sp)
	sw $t4, 16($sp)
	sw $t5, 12($sp)
	sw $t6, 8($sp)
	sw $t7, 4($sp)
	sw $ra, 0($sp)

	jal NUMEROINSTRINGA # Chiamo la procedura

	lw $t0, 32($sp) # Recupero i dati dallo stack (Pop)
	lw $t1, 28($sp)
	lw $t2, 24($sp)
	lw $t3, 20($sp)
	lw $t4, 16($sp)
	lw $t5, 12($sp)
	lw $t6, 8($sp)
	lw $t7, 4($sp)
	lw $ra, 0($sp)

	add $sp, $sp, 36

	# $v0 = base address della stringa risultante
	# $v1 = numero di caratteri della stringa risultante

	# CONTROLLO

	move $t0, $v0 # Copio $v0 in $t0

	li $v0, 4 # Stampo stringa
	la $a0, string19
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string47
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string48
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $v1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string49
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string50
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $v0, $t0 # Copio $t0 in $v0  

	# FINE CONTROLLO

	# Devo restituire i seguenti parametri

	# $v0 = stringa contenente il risultato della divisione
	# $v1 = lunghezza della stringa
	# $a0 = numero di caratteri del primo operando
	# $a1 = numero di caratteri del secondo operando

	move $a0, $t1 # Copio $t1 in $v0
	move $a1, $t2 # Copio $t2 in $v1
	# $v0 = stringa contenete il risultato della divisione
	# $v1 = numero di caratteri della stringa

	jr $ra # Esco dalla procedura

	DENOMINATORE0:

	li $v0, 4 # Stampo stringa
	la $a0, string59
	syscall

	j Exit # Esco dal programma


SOMMA:

	# $a0 = Posizione relativa del simbolo operazione nella stringa
	# $a1 = Base address della stringa

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $ra, 0($sp)

	jal PREPAROOPERAZIONE # Chiamo la procedura

	lw $ra, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	# Dalla procedura ottengo i seguenti registri:

	# $v0 = numero caratteri del primo operando
	# $v1 = numero caratteri del secondo operando
	# $a0 = primo operando in forma di intero (numero)
	# $a1 = secondo operando in forma di intero (numero)

	add $t0, $a0, $a1 # Salvo in $t0 il risultato della somma tra $a0 e $a1

	move $a0, $t0 # Copio $t0 in $a0

	move $t1, $v0 # $t1 = numero caratteri del primo operando
	move $t2, $v1 # $t2 = numero di caratteri del secondo operando

	sub $sp, $sp, 36 # Mi servono più registri: salvo nello stack i registri s dato che per convenzione la procedura non deve toccarli ma può utilizzare solo i registri temporanei

	sw $t0, 32($sp) # Push
	sw $t1, 28($sp)
	sw $t2, 24($sp)
	sw $t3, 20($sp)
	sw $t4, 16($sp)
	sw $t5, 12($sp)
	sw $t6, 8($sp)
	sw $t7, 4($sp)
	sw $ra, 0($sp)

	jal NUMEROINSTRINGA # Chiamo la procedura

	lw $t0, 32($sp) # Recupero i dati dallo stack (Pop)
	lw $t1, 28($sp)
	lw $t2, 24($sp)
	lw $t3, 20($sp)
	lw $t4, 16($sp)
	lw $t5, 12($sp)
	lw $t6, 8($sp)
	lw $t7, 4($sp)
	lw $ra, 0($sp)

	add $sp, $sp, 36

	# $v0 = base address della stringa risultante
	# $v1 = numero di caratteri della stringa risultante

	# CONTROLLO

	move $t0, $v0 # Copio $v0 in $t0

	li $v0, 4 # Stampo stringa
	la $a0, string19
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string51
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string52
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $v1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string53
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string54
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $v0, $t0 # Copio $t0 in $v0  

	# FINE CONTROLLO

	# Devo restituire i seguenti parametri
	# $v0 = stringa contenente il risultato della somma
	# $v1 = lunghezza della stringa
	# $a0 = numero di caratteri del primo operando
	# $a1 = numero di caratteri del secondo operando

	move $a0, $t1 # Copio $t1 in $v0
	move $a1, $t2 # Copio $t2 in $v1
	# $v0 = stringa contenete il risultato della somma
	# $v1 = numero di caratteri della stringa

	jr $ra # Esco dalla procedura

SOTTRAZIONE:

	# $a0 = Posizione relativa del simbolo operazione nella stringa
	# $a1 = Base address della stringa

	addi $sp, $sp, -4 # Salvo $ra nello stack prima di chiamare la procedura annidata
	sw $ra, 0($sp)

	jal PREPAROOPERAZIONE # Chiamo la procedura

	lw $ra, 0 ($sp) # Ripristino lo stack
	addi $sp, $sp, 4

	# Dalla procedura ottengo i seguenti registri:

	# $v0 = numero caratteri del primo operando
	# $v1 = numero caratteri del secondo operando
	# $a0 = primo operando in forma di intero (numero)
	# $a1 = secondo operando in forma di intero (numero)

	sub $t0, $a0, $a1 # Salvo in $t0 il risultato della differenza tra $a0 e $a1

	move $a0, $t0 # Copio $t0 in $a0

	move $t1, $v0 # $t1 = numero caratteri del primo operando
	move $t2, $v1 # $t2 = numero di caratteri del secondo operando

	sub $sp, $sp, 36 # Mi servono più registri: salvo nello stack i registri s dato che per convenzione la procedura non deve toccarli ma può utilizzare solo i registri temporanei

	sw $t0, 32($sp) # Push
	sw $t1, 28($sp)
	sw $t2, 24($sp)
	sw $t3, 20($sp)
	sw $t4, 16($sp)
	sw $t5, 12($sp)
	sw $t6, 8($sp)
	sw $t7, 4($sp)
	sw $ra, 0($sp)

	jal NUMEROINSTRINGA # Chiamo la procedura

	lw $t0, 32($sp) # Recupero i dati dallo stack (Pop)
	lw $t1, 28($sp)
	lw $t2, 24($sp)
	lw $t3, 20($sp)
	lw $t4, 16($sp)
	lw $t5, 12($sp)
	lw $t6, 8($sp)
	lw $t7, 4($sp)
	lw $ra, 0($sp)

	add $sp, $sp, 36

	# $v0 = base address della stringa risultante
	# $v1 = numero di caratteri della stringa risultante

	# CONTROLLO

	move $t0, $v0 # Copio $v0 in $t0

	li $v0, 4 # Stampo stringa
	la $a0, string19
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string55
	syscall

	li $v0, 4 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string56
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $v1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string57
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string58
	syscall

	li $v0, 1 # Stampo intero
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $v0, $t0 # Copio $t0 in $v0  

	# FINE CONTROLLO

	# Devo restituire i seguenti parametri
	# $v0 = stringa contenente il risultato della somma
	# $v1 = lunghezza della stringa
	# $a0 = numero di caratteri del primo operando
	# $a1 = numero di caratteri del secondo operando

	move $a0, $t1 # Copio $t1 in $v0
	move $a1, $t2 # Copio $t2 in $v1
	# $v0 = stringa contenete il risultato della sottrazione
	# $v1 = numero di caratteri della stringa

	jr $ra # Esco dalla procedura

PREPAROOPERAZIONE:

	PreCiclo7a:

	# $a0 = posizione relativa del simbolo operazione nella stringa
	# $a1 = base address della stringa

	# CONTROLLO

	move $t0, $a0 # Copio in $t0 la posizione relativa del simbolo operazione nella stringa
	move $t1, $a1 # Copio in $t1 il base address della stringa

	li $v0, 4 # Stampo stringa
	la $a0, string23
	syscall

	li $v0, 1
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string24
	syscall

	li $v0, 4
	move $a0, $t1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $a0, $t0 # Copio $t0 in $a0
	move $a1, $t1 # Copio $t1 in $a1

	# FINE CONTROLLO

	move $a3, $a1 # Copio il base address della stringa in $a3
	add $a0, $a0, $a1 # Ottengo la posizione assoluta del simbolo operazione nella stringa
	move $a1, $a0 # Copio la posizione assoluta del simbolo operazione nella stringa in $a1

	li $t0, 10 # Carico in $t0 il numero 10
	li $t1, 58 # Carico in $t1 il numero 58
	li $t2, 47 # Carico in $t2 il numero 47
	li $t3, 1 # 10^0
	li $t6, 0 # Inizializzo registro $t6
	li $t7, 0 # Contatore per contare il numero di cifre del primo operando

	Ciclo7a:

	addi $a0, $a0, -1 # Punto al carattere precedente al simbolo dell'operando

	lb $t4, 0($a0) # Carico in $t0 i caratteri precedenti al simbolo dell'operando

	# Devo verificare che il codice ASCII sia corrispondente ad un numero: quindi è minore di 58 e contemporaneamente il numero 47 è minore di esso altrimenti vado a FineCiclo

	slt $t5, $t4, $t1 # Se $t4 è minore di $t1 allora $t5 = 1 altrimenti $t5 = 0
	beq $t5, $zero, FineCiclo7a # Se $t5 = 0 salto a FineCiclo7a

	slt $t5, $t2, $t4 # Se $t2 è minore di $t4 allora $t5 = 1
	beq $t5, $zero, FineCiclo7a

	addi $t4, $t4, -48 # Ottengo il numero corrispondente al carattere ASCII
	mul $t4, $t4, $t3 # Moltiplico il numero per 10^n

	mul $t3, $t3, $t0 # $t3 = $t3 * $t0 # Esponente della 10

	addi $t7, $t7, 1 # Aumento il contatore di 1

	add $t6, $t6, $t4 # Sommo le varie cifre con valore posizionale per ottenere il numero intero

	beq $a0, $a3, FineCiclo7a # Se il carattere appena salvato era anche il primo della stringa allora non posso andare avanti, perciò esco

	j Ciclo7a # Salto a Ciclo7a

	FineCiclo7a:

	move $v0, $t7 # Copio $t7 in $v0 # Ora in $v0 c'è il numero di caratteri del primo operando
	move $a0, $t6 # Copio $t6 in $a0 # Ora in $a0 il primo operando in forma di intero (numero)

	PreCiclo7b:

	# $a1 = Posizione assoluta del simbolo operazione nella stringa in $a1
	# $t0 = 10
	# $t1 = 58
	# $t2 = 47
	li $t3, 1 # 10^0
	li $t6, 0 # Inizializzo registro $t6
	li $t7, 0 # Contatore per contare il numero di cifre del secondo operando

	move $a2, $a1 # Copio posizione assoluta del simbolo operazione in $a2

	Ciclo7b:

	addi $a1, $a1, 1 # Punto al carattere successivo del simbolo operazione

	lb $t4, 0($a1) # Carico in $t4 i carattere successivi al simbolo dell'operando

	# Devo verificare che il codice ASCII sia corrispondente ad un numero: quindi è minore di 58 e contemporaneamente il numero 47 è minore di esso altrimenti vado a FineCiclo

	slt $t5, $t4, $t1 # Se $t4 è minore di $t1 allora $t5 = 1 altrimenti $t5 = 0
	beq $t5, $zero, FineCiclo7b

	slt $t5, $t2, $t4 # Se $t2 è minore di $t4 allora $t5 = 1
	beq $t5, $zero, FineCiclo7b

	addi $t7, $t7, 1 # Aumento il contatore di 1

	j Ciclo7b # Salto a Ciclo7b

	FineCiclo7b:

	move $a3, $a2 # Copio in $a3 la posizione assoluta del simbolo operazione

	add $a2, $a2, $t7 # Calcolo in $a2 la posizione assoluta della cifra unità del secondo operando

	Ciclo7c: # (Oppure seconda parte del Ciclo7b)

	lb $t4, 0($a2) # Carico in $t4 le cifre del secondo operando a partire dalla cifra delle unità

	addi $t4, $t4, -48 # Ottengo il numero corrispondente al carattere ASCII
	mul $t4, $t4, $t3 # Moltiplico il numero per 10^n

	mul $t3, $t3, $t0 # $t3 = $t3 * $t0 # Esponente della 10

	add $t6, $t6, $t4 # Sommo le varie cifre con valore posizionale per ottenere il numero intero

	addi $a2, $a2, -1 # Punto al carattere precedente

	beq $a2, $a3, FineCiclo7c # Se il carattere che sto analizzando corrisponde al simbolo operazione ho finito

	j Ciclo7c # Salto a Ciclo7c

	FineCiclo7c:

	move $v1, $t7 # Copio in $v1 il numero di caratteri del secondo operando
	move $a1, $t6 # Copio in $a1 il secondo operando in forma di intero

	# CONTROLLO

	move $t0, $v0 # Copio $v0 in $t0
	move $t1, $v1 # Copio $v1 in $t1
	move $t2, $a0 # Copio $a0 in $t2
	move $t3, $a1 # Copio $a1 in $t3

	li $v0, 4 # Stampo stringa
	la $a0, string25
	syscall

	li $v0, 1 # Stampo numero
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string26
	syscall

	li $v0, 1 # Stampo numero
	move $a0, $t1
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string27
	syscall

	li $v0, 1 # Stampo numero
	move $a0, $t2
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	li $v0, 4 # Stampo stringa
	la $a0, string28
	syscall

	li $v0, 1 # Stampo numero
	move $a0, $t3
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $v0, $t0 # Copio $t0 in $v0
	move $v1, $t1 # Copio $t1 in $v1
	move $a0, $t2 # Copio $t2 in $a0
	move $a1, $t3 # Copio $t3 in $a1

	# FINE CONTROLLO

	# $v0 = numero caratteri del primo operando
	# $v1 = numero caratteri del secondo operando
	# $a0 = primo operando in forma di intero (numero)
	# $a1 = secondo operando in forma di intero (numero)

	jr $ra # Esco dalla procedura

NUMEROINSTRINGA:

	# $a0 = numero da convertire
	# Restituisce in $v0 la stringa contenente il numero sotto forma di stringa

	# CONTROLLO

	move $t0, $a0 # Copio $a0 in $t0

	li $v0, 4 # Stampo stringa
	la $a0, string20
	syscall

	li $v0, 1 # Stampo stringa
	move $a0, $t0
	syscall

	addi $a0, $0, 0xA # Stampo linea vuota
	addi $v0, $0, 0xB
	syscall

	move $a0, $t0 # Copio $t0 in $a0

	# FINE CONTROLLO

	move $t6, $a0 # Copio $a0 in $t6 perché sporcherò $a0

	PreCiclo6:

	li $v0, 9 # Richiedo spazio in memoria
	li $a0, 50
	syscall
	# $v0 = indirizzo della nuova memoria

	move $t5, $v0 # Copio $v0 in $t5 perché dovrò modificarlo
	addi $t5, $t5, 50 # Aggiungo 50 a $t5

	li $t1, 10 # Per effettuare la divisione /10
	li $t7, 0 # Azzero il contatore

	Ciclo6:

	div $t6, $t1 # Divido $t6 e $t1
	mfhi $t0 # Sposto il resto della divisione in $t0
	mflo $t2 # Sposto in $t2 il risultato della divisione

	move $t6, $t2 # Sposto $t2 in $t6 per il prossimo ciclo

	addi $t0, $t0, 48 # Faccio diventare $t0 il codice ASCII corrispondente al numero
	sb $t0, 0($t5) # Posiziono il carattere nella stringa

	addi $t7, $t7, 1 # Aumento il contatore di 1

	addi $t5, $t5, -1 # Punto al carattere precedente

	slt $t4, $t2, $t1 # Se $t2 < 10 allora $t4 = 1
	beq $t4, $zero, Ciclo6 # Se $t4 = 0 salto a Ciclo6

	FineCiclo6:

	beq $t2, $zero, FineAlternativa # Nel caso in cui il numero in input è di una cifra si andrebbe a scrivere "04". Per questo inseriamo fine alternativa.

	addi $t2, $t2, 48 # Faccio diventare $t2 il codice ASCII corrispondente al numero

	sb $t2, 0($t5) # Posiziono l'ultima resto
	addi $t7, $t7, 1 # Aumento il contatore

	move $v0, $t5 # Copio in $v0 il base address della stringa
	# $v0 = base address della stringa risultante
	move $v1, $t7 # Numero di caratteri della stringa

	jr $ra # Esco dalla procedura

	FineAlternativa:

	addi $t5, $t5, 1 # Punto al carattere successivo perché non ho scritto nessun carattere

	move $v0, $t5 # Copio in $v0 il base address della stringa
	# $v0 = base address della stringa risultante
	move $v1, $t7 # Copio $t7 in $v1 (numero di caratteri della stringa)

	jr $ra # Esco dalla procedura

EscoDaCalcolatore:

	# Devo passare in $v0 il risultato sotto forma di stringa e in $v1 la lunghezza della stringa

	lw $s0, 32($sp) # Recupero i dati dallo stack (Pop)
	lw $s1, 28($sp)
	lw $s2, 24($sp)
	lw $s3, 20($sp)
	lw $s4, 16($sp)
	lw $s5, 12($sp)
	lw $s6, 8($sp)
	lw $s7, 4($sp)
	lw $ra, 0($sp)

	add $sp, $sp, 36

	jr $ra # Esco dalla procedura

Exit: # Esco dal programma

	li $v0, 10
	syscall


