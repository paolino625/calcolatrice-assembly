// Calcolatrice che da precedenza alla moltiplicazione e alla divisione. (Accetta come input numeri decimali)

import java.util.ArrayList;

public class Calcolatrice3{

	public static void main(String[] args){

		double numerofinale = 0;
		double numeronuovo = 0;

		ArrayList<String> elenco = new ArrayList<String>();

		for (int i = 0; i < args.length; i++){

			elenco.add(""+args[i]+"");

		}		

		for (int i = 0; i < elenco.size(); i++){

			if (elenco.get(i).equals("*")){

				double numeroprima = Double.parseDouble(elenco.get(i-1));
				double numerodopo = Double.parseDouble(elenco.get(i+1));

				numerofinale = numeroprima * numerodopo;

				elenco.remove(i-1);
				elenco.remove(i-1);
				elenco.remove(i-1);

				elenco.add(i-1, String.valueOf(numerofinale));

				i = 0;

			}

			if (elenco.get(i).equals("/")){

				double numeroprima = Double.parseDouble(elenco.get(i-1));
				double numerodopo = Double.parseDouble(elenco.get(i+1));

				numerofinale = numeroprima / numerodopo;

				elenco.remove(i-1);
				elenco.remove(i-1);
				elenco.remove(i-1);

				elenco.add(i-1, String.valueOf(numerofinale));

				i = 0;

			}

		}

		System.out.println("Svolgo le operazioni di moltiplicazione e divisione: ");

		for (String elemento: elenco){

			System.out.print(""+elemento+" ");

		}

		System.out.println();

		numerofinale = 0;

		for (int i = 0; i < elenco.size(); i++){


			if (elenco.get(i).equals("+") == false && elenco.get(i).equals("-") == false && elenco.get(i).equals("*") == false && elenco.get(i).equals("/") == false){

				numeronuovo = 	Double.parseDouble(elenco.get(i));

				if (i != 0){

					if (elenco.get(i-1).equals("+") == true){

						numerofinale += numeronuovo;

					}

					if (elenco.get(i-1).equals("-") == true){

						numerofinale -= numeronuovo;

					}

					/* if (elenco.get(i-1).equals("*") == true){

						numerofinale = numerofinale*numeronuovo;

					}

					if (elenco.get(i-1).equals("/") == true){

						numerofinale = numerofinale/numeronuovo;

					} */

				}

				else{

					numerofinale = numeronuovo;

				}

			}

		}

		numerofinale = Math.round(numerofinale);
		int numerofinaleintero = (int)numerofinale;
		System.out.println("La soluzione è: "+numerofinaleintero+"");

	}
	
}