## Documenti

- [Specifica Progetto](https://gitlab.com/paolino625/calcolatrice-assembly/-/blob/master/Documenti/Specifica%20Progetto.pdf)
- [Documentazione](https://gitlab.com/paolino625/calcolatrice-assembly/-/blob/master/Documenti/Documentazione.pdf)

## Codice

- [Calcolatrice - Assembly](https://gitlab.com/paolino625/calcolatrice-assembly/-/blob/master/Codice/Calcolatrice%20-%20Assembly.asm)
- [Calcolatrice - Java Proof of Concept](https://gitlab.com/paolino625/calcolatrice-assembly/-/blob/master/Codice/Calcolatrice%20-%20Java%20Proof%20of%20Concept.java)